<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

// Route::get('/', function () {
//     return view('index');
// });

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'Site\HomeController@index');
Route::get('/addImage', 'Site\ImageController@create');
Route::post('/addImage', 'Site\ImageController@store');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => 'auth'], function() {
Route::get('/albums', 'AlbumController@index')->name('album');
Route::get('/album/add', 'AlbumController@create');
Route::post('/album', 'AlbumController@store');
Route::get('/album/edit/{id}', 'AlbumController@edit');
Route::put('/album/update/{id}', 'AlbumController@update');
Route::delete('/album/delete/{id}', 'AlbumController@destroy');
});


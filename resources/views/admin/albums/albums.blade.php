@extends('layouts.app')
@section('extra-css')
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css" integrity="sha256-xykLhwtLN4WyS7cpam2yiUOwr709tvF3N/r7+gOMxJw=" crossorigin="anonymous" />
@endsection
@section('content')
<div class="container">
    <div class="row">
        @include('admin.includes.sidebar')
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">Albums</div>
                <div class="panel-body">
                    <div class="col-md-12">
                        <a href="{!! url('/album/add') !!}" class="btn btn-primary">Add Album <i class="fas fa-plus"></i></a>
                    </div>
                    <br>
                    <br>
                    <table id="example" class="table-hover table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($albums as $album)
                            <tr>
                                <td>{!! $album->name !!}</td>
                                <td>
                                    <a href="{!! url('/album/edit/'.$album->id)!!}" class><i class="fas fa-edit"></i></a>
                                    <a data="{!! $album->id !!}" class="delete"><i class="fas fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@include('admin.includes.deleteModal')
@endsection
@section('extra-js')
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js" integrity="sha256-3blsJd4Hli/7wCQ+bmgXfOdK7p/ZUMtPXY08jmxSSgk=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function () {
        $('#example').DataTable();
        $('.delete').click(function () {
            $('#delete-modal').modal('show');
            $('.delete-record').attr('data-id', $(this).attr('data'));
        });
        $('.delete-record').click(function () {
            $.ajax({
                url: '{!! url("/album/delete") !!}/'+$(this).attr('data-id'),
                type: 'delete',
                data: {'_token': '{!! csrf_token() !!}'},
                success(data) {
                    if (data.success !== 'undefined') {
                        toastr.success(data.success);
                    } else {
                        toastr.error(data.error);
                    }
                    window.location.reload();
                }
            });
        });
    });
</script>
@endsection
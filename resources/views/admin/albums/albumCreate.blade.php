@extends('layouts.app')
@section('extra-css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css" integrity="sha256-xykLhwtLN4WyS7cpam2yiUOwr709tvF3N/r7+gOMxJw=" crossorigin="anonymous" />
@endsection
@section('content')
<div class="container">
    <div class="row">
        @include('admin.includes.sidebar')
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">Create Album</div>

                <div class="panel-body">
                    {!! Form::open(['method' => 'post','class'=>'form-horizontal', 'id'=>'add-album' ,'action'=>['AlbumController@store']]) !!}
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        {!! Form::label('name', 'Name', ['class' => 'col-md-3 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::text('name', null, ['class' => 'form-control','id'=>'name'])!!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('extra-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js" integrity="sha256-3blsJd4Hli/7wCQ+bmgXfOdK7p/ZUMtPXY08jmxSSgk=" crossorigin="anonymous"></script>
<script>
    $('#add-album').submit(function (event) {
        event.preventDefault();
        var name = $('#name').val();
        $.ajax({
            url: '{!! url("/album") !!}',
            type: 'post',
            data: {'_token': '{!! csrf_token() !!}', name:name},
            success(data) {
                if(data.success !== 'undefined' ){
                    toastr.success(data.success);
                }else{
                    toastr.error(data.error);
                }
            }
        });
    });

</script>
@endsection

@extends('public.main')
@section('content')
	{!! Form::open(['method'=>'POST','action'=>'Site\ImageController@store','id'=>'form-id','class'=>'form-horizontal','files'=>true]) !!}
		<div class="form-group col-sm-12 fleft">
			{!! Form::label('image','Upload Your Image',['id'=>'label-id','class'=>'col-sm-3 fleft control-label']) !!}
			<div class="col-sm-6 fleft">
				{!! Form::file('img',['id'=>'img','class'=>'form-control','accept' => '.jpg, .png','required'=>'required']) !!}
			</div>
		</div>
		<div class="form-group col-sm-12 fleft">
			{!! Form::label('submit','Submit',['id'=>'label-id','class'=>'col-sm-3 fleft control-label']) !!}
			<div class="col-sm-6 fleft">
				{!! Form::submit('Upload',['class'=>'btn btn-primary']) !!}
			</div>
		</div>
	{!! Form::close() !!}
@endsection
@extends('layouts.app')
@section('extra-css')
<link href="/css/basic.css" rel="stylesheet">
@endsection
@section('content')
<div class="flipbook-viewport">
                <div class="container">
                    <div class="flipbook">
                        <div style="background-image:url(/pages/1.jpg)"></div>
                        <div style="background-image:url(/pages/2.jpg)"></div>
                        <div style="background-image:url(/pages/3.jpg)"></div>
                        <div style="background-image:url(/pages/4.jpg)"></div>
                        <div style="background-image:url(/pages/5.jpg)"></div>
                        <div style="background-image:url(/pages/6.jpg)"></div>
                        <div style="background-image:url(/pages/7.jpg)"></div>
                        <div style="background-image:url(/pages/8.jpg)"></div>
                        <div style="background-image:url(/pages/9.jpg)"></div>
                        <div style="background-image:url(/pages/10.jpg)"></div>
                        <div style="background-image:url(/pages/11.jpg)"></div>
                        <div style="background-image:url(/pages/12.jpg)"></div>
                    </div>
                </div>
            </div>
@endsection
@section('extra-js')
<script src="/js/jquery.min.1.7.js" type="text/javascript"></script>
<script src="/js/modernizr.2.5.3.min.js" type="text/javascript"></script>
<script src="/js/turn.js" type="text/javascript"></script>
<script src="/js/turn.html4.min.js" type="text/javascript"></script>
<script type="text/javascript">

function loadApp() {

    // Create the flipbook

    $('.flipbook').turn({
        // Width

        width: 1300,

        // Height

        height: 600,

        // Elevation

        elevation: 1,

        // Enable gradients

        gradients: true,

        // Auto center this flipbook

        autoCenter: true

    });
}

// Load the HTML4 version if there's not CSS transform
yepnope({
    test: Modernizr.csstransforms,
    yep: ['/js/turn.js'],
    nope: ['/js/turn.html4.min.js'],
    both: ['/css/basic.css'],
    complete: loadApp
});


</script>
@endsection

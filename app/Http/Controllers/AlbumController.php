<?php

namespace App\Http\Controllers;

use App\Album;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AlbumController extends Controller {

    public function validateAlbum($data) {
        $rules = [
            'name' => 'string|required',
            'user_id' => 'integer|required',
            'settings' => 'array'
        ];
        $validator = Validator::make($data, $rules);
        return $validator;
    }

    public function index() {
        $albums = Album::all();
        $data = [];
        foreach ($albums as $album) {
            $edit = "<a href='".url('/album/edit/'.$album->id)."'><i class='fas fa-edit'></i></a>";
            $data[] = [
                'id' => $album->id,
                'name' => $album->name,
                'action' => $edit,
            ];
        }
        return view('admin.albums.albums', ['albums' => $albums]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.albums.albumCreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $request->merge(['user_id' => Auth::user()->id]);
        $validator = $this->validateAlbum($request->all());
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()]);
        } else {
            $store = Album::create($request->all());
            if ($store) {
                return response()->json(['success' => 'Album Created Successfully.']);
            } else {
                return response()->json(['error' => 'Album not created.']);
            }
        }
    }

    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $album = Album::find($id);
        if ($album) {
            return view('admin.albums.albumEdit', ['album' => $album]);
        } else {
            return response(['error' => 'Album not found']);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $album = Album::find($id);
        if ($album) {
            $update = $album->update($request->all());
            if ($update) {
                return response(['success' => 'Album updated.']);
            } else {
                return response(['error' => 'Album not updated.']);
            }
        } else {
            return response(['error' => 'Album not found']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $album = Album::find($id);
        if ($album) {
            $delete = $album->delete();
            if ($delete) {
                return response(['success' => 'Album deleted.']);
            } else {
                return response(['error' => 'Album not deleted.']);
            }
        } else {
            return response(['error' => 'Album not found']);
        }
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    protected $fillable = ['user_id', 'name', 'settings'];
    
    public function createdBy(){
        return $this->belongsTo('\App\User');
    }
}
